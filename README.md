# desafio-estagiario-2

### Desafio RPE de Seleção 
Olá, queremos convidá-lo a participar de nosso desafio de seleção.  Pronto para participar? Seu trabalho será visto por nosso time e você receberá ao final um feedback sobre o que achamos do seu trabalho. Não é legal?

### Sobre a oportunidade 
A vaga é para Estágio em Desenvolvimento Java.

### Desafio Técnico

  - Resumo do problema: Precisamos manter um cadastro das lojas de nossos clientes para futuras consultas. Eles possuem lojas físicas e virtuais com informações diferentes entre elas. Quando precisamos de um contato de telefone ou endereço, temos que buscar nas pastas físicas de cadastros de clientes.
    
  
  - Dicionário:
    ```
    * Loja Física: Loja física são lojas localizadas em um endereço de correspondência único.
      Atributos: CNPJ, Nome, Segmento, Telefone, Endereco Físico, Número de Funcionários
        
    * Loja Virtual: Loja virtual são lojas hospedadas em ambientes/sites.
      Atributos: CNPJ, Nome, Segmento, Telefone, URL, Avaliação
    ```

  Objetivo do Desafio: Desenvolver uma api que tenha uma função de CRUD para manter os cadastros das lojas.    
  
    
  - Pré-requisitos:
    ```
    * Utilização de banco de dados Oracle, MySQL, H2, Postgres ou qualquer outro banco relacional.
    * Java 17+
    * Maven
    * Swagger

    ```

  - O que esperamos como escopo:
    ```
    * Endpoint para adicionar uma Loja Física
    * Endpoint para adicionar uma Loja Virtual
    * Endpoint para consultar uma Loja Física
    * Endpoint para consultar uma Loja Virtual
    * Endpoint para alterar uma Loja Física
    * Endpoint para alterar uma Loja Virtual
    * Endpoint para remover uma Loja Física
    * Endpoint para remover uma Loja Virtual

    ```

  - Não precisa desenvolver:
    ```
    * Front
    ```

  - Extra/Bônus (não Obrigatório)
    ```
    * Implementar testes unitários
    ```
  
  - O que vamos avaliar:
    ```
    * Organização de código;
    * Funcionamento;
    * Boas práticas;
    ```

### Instruções
        1. Crie um projeto no gitlab ou github;
        2. Desenvolva. Você terá 5 (cinco) dias a partir da data do envio do desafio; 
        3. Crie um arquivo de texto com a nomenclatura README.md com a explicação de como devemos executar o 
        projeto e com uma descrição do que foi feito; 
        4. Envie o link do projeto respondendo ao email enviado.

